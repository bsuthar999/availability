import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AvailabilityDataSource } from './availability-datasource';
import { AvailabilityService } from '../service/availability.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: AvailabilityDataSource;
  displayedColumns = [
    'sr_no',
    'user',
    'fromTime',
    'toTime',
    '_id',
  ];
  filterForm: FormGroup;
  avForm: FormGroup;
  startDate = new Date();

  constructor(
    private formBuilder: FormBuilder,
    private availabilityService: AvailabilityService,
    private readonly snackBar : MatSnackBar
    ) { }

  ngOnInit() {
    this.dataSource = new AvailabilityDataSource(this.availabilityService);
    this.dataSource.loadItems(undefined, undefined, undefined, {})

    this.setAvForm()
    this.setFilterForm()

    this.filterForm.controls.forDate.valueChanges.pipe(debounceTime(300)).subscribe({
      next: data => {
        this.setFilter()
      }
    });

    this.filterForm.controls.forUser.valueChanges.pipe(debounceTime(300)).subscribe({
      next: data => {
        this.setFilter()
      }
    });
  }

  setAvForm(){
    this.avForm = this.formBuilder.group({
      userEmailFormControl : new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      fromTimeSlot: new FormControl('',[
      ]),
      fromDate: new FormControl('',[
        Validators.required,
      ]),
      toTimeSlot: new FormControl('',[
        Validators.required,
      ]),
      toDate:  new FormControl('',[
        Validators.required,
      ]),
    });

    this.avForm.controls.fromDate.setValue(this.startDate)
    this.avForm.controls.fromTimeSlot.setValue('00:00 AM')
    this.avForm.controls.toTimeSlot.setValue('11:59 PM')
    this.avForm.controls.toDate.setValue(this.startDate)
  }

  setFilterForm(){
    this.filterForm = this.formBuilder.group({
      forDate : new FormControl('',[]),
      forUser: new FormControl('',[]),
    })

  }

  setFilter(event?) {
    const query: any = {};
    if (this.filterForm.controls.forUser.value) query.search = this.filterForm.controls.forUser.value;

    if (this.filterForm.controls.forDate.value) {
      query.fromTime = new Date(this.filterForm.controls.forDate.value).setHours(
        0,
        0,
        0,
        0,
      );
      query.toTime = new Date(this.filterForm.controls.forDate.value).setHours(
        23,
        59,
        59,
        59,
      );
    }

    let sortQuery = {};
    if (event) {
      for (const key of Object.keys(event)) {
        if (key === 'active' && event.direction !== '') {
          sortQuery[event[key]] = event.direction;
        }
      }
    }

    sortQuery = Object.keys(sortQuery).length === 0 ? { _id : 'DESC' } : sortQuery;

    this.dataSource.loadItems(
      sortQuery,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
    );
  }

  addAvailability(){
    if(this.avForm.valid){
      const availability : Availability= {}
      const fromTimeHM = this.getTime(this.avForm.controls.fromTimeSlot.value)
      const toTimeHM = this.getTime(this.avForm.controls.toTimeSlot.value)

      const fromTime = new Date(this.avForm.controls.fromDate.value).setHours(fromTimeHM.hours,fromTimeHM.minutes)
      const toTime = new Date(this.avForm.controls.toDate.value).setHours(toTimeHM.hours,toTimeHM.minutes)

      availability.user = this.avForm.controls.userEmailFormControl.value;
      availability.fromTime = new Date(fromTime).getTime()
      availability.toTime = new Date(toTime).getTime()

      this.availabilityService.addAvailability(availability).subscribe({
        next: success => {
            this.snackBar.open("Availability added successfully!.",'Close', { duration :2500 })
        },
        error: err => {
          this.snackBar.open(err.error.message,'Close', { duration :2500 })
        }
      })
    }else{
      this.validateAllFormFields(this.avForm)
    }
  }

  getTime(time:string){
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];

    if(AMPM == "PM" && hours<12) hours = hours+12;
    if(AMPM == "AM" && hours==12) hours = hours-12;

    var sHours = hours.toString();
    var sMinutes = minutes.toString();

    if(hours<10) sHours = "0" + sHours;
    if(minutes<10) sMinutes = "0" + sMinutes;

    return {
      hours : Number(sHours),
      minutes: Number(sMinutes)
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);
    if (control instanceof FormControl) {
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {
      this.validateAllFormFields(control);
    }
  });
}

}


export class Availability {
  user?: string;
  fromTime?: number;
  toTime?: number;
}
