import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { map, catchError, finalize, switchMap } from 'rxjs/operators';
import { BehaviorSubject, Observable, of, from } from 'rxjs';
import { AvailabilityService } from '../service/availability.service';

export interface ListingData {
  _id: string;
  fromSlot: Date;
  toSlot: Date;
  forDate: string;
  user: string;
}

export interface ListResponse {
  docs: ListingData[];
  length: number;
  offset: number;
}
export class AvailabilityDataSource extends DataSource<ListingData> {
  data: ListingData[];
  length: number;
  offset: number;

  itemSubject = new BehaviorSubject<ListingData[]>([]);
  loadingSubject = new BehaviorSubject<boolean>(false);

  loading$ = this.loadingSubject.asObservable();

  constructor(private availabilityService: AvailabilityService) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<ListingData[]> {
    return this.itemSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.itemSubject.complete();
    this.loadingSubject.complete();
  }

  loadItems(sortOrder?, pageIndex = 0, pageSize = 10, query?) {
    this.loadingSubject.next(true);
    this.availabilityService
      .getAvailabilityList(sortOrder, pageIndex, pageSize, query)
      .pipe(
        map((res: ListResponse) => {
          this.data = res.docs;
          this.offset = res.offset;
          this.length = res.length;
          return res.docs;
        }),
        catchError(() => of([])),
        switchMap((items) => {
          items.forEach(item => {
            item.fromSlot = new Date(item.fromTime).toLocaleString('en-US', {timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone})
            item.toSlot = new Date(item.toTime).toLocaleString('en-US', {timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone})
          })
          return of(items);
        }),
        finalize(() => this.loadingSubject.next(false)),
      )
      .subscribe((items : any) => {
        this.itemSubject.next(items);
      });
  }

}
