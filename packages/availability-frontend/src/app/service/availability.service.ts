import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Availability } from '../home/home.component';

@Injectable({
  providedIn: 'root'
})
export class AvailabilityService {

  SERVER_URL = 'http://localhost:3000';
  // ideally we could add a proxy to backend server and just relay something like '/api' to http://localhost:300
  // from proxy config.

  constructor(private http: HttpClient) { }

  addAvailability(availability:Availability){
    const url = '/availability/v1/create';

    return this.http.post(this.SERVER_URL + url, availability)
  }

  getAvailabilityList(sortOrder, pageNumber = 0, pageSize = 10, query) {
    if (!sortOrder) sortOrder = { _id: 'asc' };
    if (!query) query = {};

    try {
      sortOrder = JSON.stringify(sortOrder);
    } catch (error) {
      sortOrder = JSON.stringify({ _id: 'asc' });
    }

    const url = '/availability/v1/list';

    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString())
      .set('sort', sortOrder)
      .set('filter_query', JSON.stringify(query));

    return this.http.get(this.SERVER_URL + url, { params });
  }

}
