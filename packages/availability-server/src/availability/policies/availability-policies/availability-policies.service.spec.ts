import { Test, TestingModule } from '@nestjs/testing';
import { AvailabilityPoliciesService } from './availability-policies.service';
import { AvailabilityService } from '../../entity/availability/availability.service';

describe('AvailabilityPoliciesService', () => {
  let service: AvailabilityPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AvailabilityPoliciesService,
        {
          provide: AvailabilityService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AvailabilityPoliciesService>(
      AvailabilityPoliciesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
