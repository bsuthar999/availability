import { Injectable, BadRequestException } from '@nestjs/common';
import { AvailabilityDto } from '../../entity/availability/availability-dto';
import { AvailabilityService } from '../../entity/availability/availability.service';
import { throwError, from, of } from 'rxjs';
import {
  FROM_TIME_SHOULD_BE_LESS_THAN_TO_TIME,
  AVAILABILITY_ALREADY_EXISTS,
} from '../../../constant/messages';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class AvailabilityPoliciesService {
  constructor(private readonly availabilityService: AvailabilityService) {}

  validateAvailability(payload: AvailabilityDto) {
    if (payload.fromTime > payload.toTime) {
      return throwError(
        new BadRequestException(FROM_TIME_SHOULD_BE_LESS_THAN_TO_TIME),
      );
    }

    return from(
      this.availabilityService.find({
        user: payload.user,
        $or: [
          {
            fromTime: { $gte: payload.fromTime },
            toTime: { $lte: payload.fromTime },
          },
          {
            fromTime: { $gte: payload.toTime },
            toTime: { $lte: payload.toTime },
          },
        ],
      }),
    ).pipe(
      switchMap(existingAvailability => {
        if (existingAvailability.length === 1) {
          if (
            payload.fromTime >= existingAvailability[0].fromTime &&
            payload.toTime <= existingAvailability[0].toTime
          ) {
            return throwError(
              new BadRequestException(AVAILABILITY_ALREADY_EXISTS),
            );
          }
        }
        return of(true);
      }),
    );
  }
}
