import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { AvailabilityService } from '../../entity/availability/availability.service';
import { AvailabilityDto } from '../../entity/availability/availability-dto';
import { from, throwError, forkJoin, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Availability } from '../../entity/availability/availability.entity';
import * as uuidv4 from 'uuid/v4';
import { AvailabilityPoliciesService } from '../../policies/availability-policies/availability-policies.service';

@Injectable()
export class AvailabilityAggregateService extends AggregateRoot {
  constructor(
    private readonly availabilityService: AvailabilityService,
    private readonly availabilityPolicy: AvailabilityPoliciesService,
  ) {
    super();
  }

  create(payload: AvailabilityDto) {
    return this.availabilityPolicy.validateAvailability(payload).pipe(
      switchMap(isValid => {
        return from(
          this.availabilityService.find({
            user: payload.user,
            $or: [
              {
                fromTime: { $lte: payload.fromTime },
                toTime: { $gte: payload.fromTime },
              },
              {
                fromTime: { $lte: payload.toTime },
                toTime: { $gte: payload.toTime },
              },
            ],
          }),
        );
      }),
      switchMap((existingAvailability: Availability[]) => {
        if (!existingAvailability || existingAvailability.length === 0) {
          return from(this.availabilityService.create(payload));
        }
        return this.validateAddAvailability(payload, existingAvailability);
      }),
    );
  }

  validateAddAvailability(
    payload: AvailabilityDto,
    existingAvailability: Availability[],
  ) {
    const availabilityHash: AvailabilityHashInterface = this.getAvailabilityHash(
      payload,
      existingAvailability,
    );

    if (availabilityHash.availabilityUuid.length === 1) {
      return this.extendExistingAvailability(availabilityHash);
    }

    return forkJoin({
      deleteAvailability: this.deleteOverlappingAvailability(availabilityHash),
      addConcatedAvailability: this.addConcatedAvailability(availabilityHash),
    }).pipe(
      switchMap(success => {
        return of({});
      }),
    );
  }

  deleteOverlappingAvailability(availabilityHash: AvailabilityHashInterface) {
    return from(
      this.availabilityService.deleteMany({
        uuid: { $in: availabilityHash.availabilityUuid },
      }),
    );
  }

  addConcatedAvailability(availabilityHash: AvailabilityHashInterface) {
    const availability = new Availability();

    availability.uuid = uuidv4();
    availability.fromTime = Math.min.apply(null, availabilityHash.timeSlotSet);
    availability.user = availabilityHash.user;
    availability.toTime = Math.max.apply(null, availabilityHash.timeSlotSet);

    return from(this.availabilityService.create(availability));
  }

  extendExistingAvailability(availabilityHash: AvailabilityHashInterface) {
    const query: any = {};
    query.fromTime = Math.min.apply(null, availabilityHash.timeSlotSet);
    query.toTime = Math.max.apply(null, availabilityHash.timeSlotSet);
    return from(
      this.availabilityService.updateOne(
        { uuid: availabilityHash.availabilityUuid[0] },
        { $set: query },
      ),
    );
  }

  getAvailabilityHash(
    payload: AvailabilityDto,
    existingAvailability: Availability[],
  ): any {
    const uuids: string[] = [];
    const timeSlot = new Set();

    timeSlot.add(payload.fromTime);
    timeSlot.add(payload.toTime);

    existingAvailability.forEach(availability => {
      uuids.push(availability.uuid);
      timeSlot.add(availability.fromTime);
      timeSlot.add(availability.toTime);
    });

    return {
      timeSlotSet: Array.from(timeSlot),
      availabilityUuid: uuids,
      user: payload.user,
    };
  }

  async getItemList(
    offset,
    limit,
    sort,
    filterQuery,
    filter,
    clientHttpRequest,
  ) {
    return this.availabilityService.list(
      offset,
      limit,
      sort,
      filterQuery,
      filter,
    );
  }
}

export interface AvailabilityHashInterface {
  timeSlotSet: string[];
  availabilityUuid: string[];
  user: string;
}
