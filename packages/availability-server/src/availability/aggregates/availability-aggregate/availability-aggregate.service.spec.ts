import { Test, TestingModule } from '@nestjs/testing';
import { AvailabilityAggregateService } from './availability-aggregate.service';
import { AvailabilityService } from '../../../availability/entity/availability/availability.service';
import { AvailabilityPoliciesService } from '../../policies/availability-policies/availability-policies.service';
describe('AvailabilityAggregateService', () => {
  let service: AvailabilityAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AvailabilityAggregateService,
        {
          provide: AvailabilityService,
          useValue: {},
        },
        {
          provide: AvailabilityPoliciesService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AvailabilityAggregateService>(
      AvailabilityAggregateService,
    );
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
