import { Module } from '@nestjs/common';
import { ItemEntitiesModule } from './entity/item-entity.module';
import { ItemController } from './controller/availability.controller';
import { AvailabilityPoliciesService } from './policies/availability-policies/availability-policies.service';
import { AvailabilityCommandManager } from './commands';
import { AvailabilityQueryManager } from './query';
import { AvailabilityAggregatesManager } from './aggregates';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [ItemEntitiesModule, CqrsModule],
  controllers: [ItemController],
  providers: [
    AvailabilityPoliciesService,
    ...AvailabilityCommandManager,
    ...AvailabilityQueryManager,
    ...AvailabilityAggregatesManager,
  ],
  exports: [ItemEntitiesModule, CqrsModule],
})
export class ItemModule {}
