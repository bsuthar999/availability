import { Test, TestingModule } from '@nestjs/testing';
import { ItemController } from './availability.controller';
import { QueryBus, CommandBus } from '@nestjs/cqrs';

describe('Item Controller', () => {
  let controller: ItemController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ItemController],
      providers: [
        { provide: QueryBus, useValue: {} },
        { provide: CommandBus, useValue: {} },
      ],
    }).compile();

    controller = module.get<ItemController>(ItemController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
