import {
  Controller,
  Req,
  Get,
  Query,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QueryBus, CommandBus } from '@nestjs/cqrs';
import { RetrieveAvailabilityListQuery } from '../query/list-availability/retrieve-availability-list.query';
import { AvailabilityDto } from '../entity/availability/availability-dto';
import { CreateAvailabilityCommand } from '../commands/create-availability/create-availability.command';

@Controller('availability')
export class ItemController {
  constructor(
    private readonly queryBus: QueryBus,
    private readonly commandBus: CommandBus,
  ) {}

  @Get('v1/list')
  async getItemList(@Query() query, @Req() clientHttpRequest) {
    const { offset, limit, sort, filter_query, search } = query;
    let filter;
    try {
      filter = JSON.parse(filter_query);
    } catch {
      filter;
    }
    return await this.queryBus.execute(
      new RetrieveAvailabilityListQuery(
        offset,
        limit,
        sort,
        search,
        filter,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async setMinimumItemPrice(@Body() body: AvailabilityDto) {
    return await this.commandBus.execute(new CreateAvailabilityCommand(body));
  }
}
