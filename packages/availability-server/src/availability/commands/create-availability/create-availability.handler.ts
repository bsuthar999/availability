import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { CreateAvailabilityCommand } from './create-availability.command';
import { AvailabilityAggregateService } from '../../../availability/aggregates/availability-aggregate/availability-aggregate.service';

@CommandHandler(CreateAvailabilityCommand)
export class CreateAvailabilityHandler
  implements ICommandHandler<CreateAvailabilityCommand> {
  constructor(
    private readonly manager: AvailabilityAggregateService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: CreateAvailabilityCommand) {
    const { payload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.create(payload).toPromise();
    aggregate.commit();
  }
}
