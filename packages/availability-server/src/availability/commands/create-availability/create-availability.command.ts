import { ICommand } from '@nestjs/cqrs';
import { AvailabilityDto } from '../../../availability/entity/availability/availability-dto';

export class CreateAvailabilityCommand implements ICommand {
  constructor(public readonly payload: AvailabilityDto) {}
}
