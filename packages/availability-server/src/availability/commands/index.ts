import { CreateAvailabilityHandler } from './create-availability/create-availability.handler';

export const AvailabilityCommandManager = [CreateAvailabilityHandler];
