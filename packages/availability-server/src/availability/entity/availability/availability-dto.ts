import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class AvailabilityDto {
  @IsNotEmpty()
  @IsString()
  user: string;

  @IsNotEmpty()
  @IsNumber()
  fromTime: number;

  @IsNotEmpty()
  @IsNumber()
  toTime: number;
}
