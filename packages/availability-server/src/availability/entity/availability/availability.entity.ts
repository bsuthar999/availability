import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Availability extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  user: string;

  @Column()
  uuid: string;

  @Column()
  fromTime: number;

  @Column()
  toTime: number;
}
