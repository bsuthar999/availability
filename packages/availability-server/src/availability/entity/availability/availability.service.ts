import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';
import { Availability } from './availability.entity';
import * as uuidv4 from 'uuid/v4';

@Injectable()
export class AvailabilityService {
  constructor(
    @InjectRepository(Availability)
    private readonly availabilityRepository: MongoRepository<Availability>,
  ) {}

  async find(query?) {
    return await this.availabilityRepository.find(query);
  }

  async create(customerPayload) {
    const customer = new Availability();
    Object.assign(customer, customerPayload);
    customer.uuid = customer.uuid ? customer.uuid : uuidv4();
    return await this.availabilityRepository.insertOne(customer);
  }

  async findOne(param, options?) {
    return await this.availabilityRepository.findOne(param, options);
  }

  async list(skip, take, sort, search?, filterQuery?) {
    let sortQuery;
    try {
      sortQuery = JSON.parse(sort);
    } catch {
      sortQuery = { name: 'asc' };
    }

    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }

    const $and: any[] = [filterQuery ? this.getFilterQuery(filterQuery) : {}];

    if (filterQuery && filterQuery.search) {
      $and.push({ user: { $regex: filterQuery.search || '' } });
    }

    const where: { $and: any } = { $and };

    const results = await this.availabilityRepository.find({
      skip,
      take,
      where,
      order: sortQuery,
    });

    return {
      docs: results || [],
      length: await this.availabilityRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.availabilityRepository.deleteOne(query, options);
  }

  async deleteMany(query, options?) {
    return await this.availabilityRepository.deleteMany(query, options);
  }

  async updateOne(query, options?) {
    return await this.availabilityRepository.updateOne(query, options);
  }

  async count(query) {
    return await this.availabilityRepository.count(query);
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    let filterQuery = {};
    if (keys && keys.length) {
      if (query.fromTime && query.toTime) {
        filterQuery = {
          fromTime: { $gte: query.fromTime },
          toTime: { $lte: query.toTime },
        };
      }
    }
    return filterQuery;
  }
}
