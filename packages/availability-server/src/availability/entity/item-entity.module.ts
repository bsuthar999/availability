import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AvailabilityService } from './availability/availability.service';
import { Availability } from './availability/availability.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Availability])],
  providers: [AvailabilityService],
  exports: [AvailabilityService],
})
export class ItemEntitiesModule {}
