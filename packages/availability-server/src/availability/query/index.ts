import { RetrieveAvailabilityListHandler } from './list-availability/retrieve-availability-list-query.handler';

export const AvailabilityQueryManager = [RetrieveAvailabilityListHandler];
