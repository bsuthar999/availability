import { IQuery } from '@nestjs/cqrs';

export class RetrieveAvailabilityListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
    public filter: string,
    public clientHttpRequest: any,
  ) {}
}
