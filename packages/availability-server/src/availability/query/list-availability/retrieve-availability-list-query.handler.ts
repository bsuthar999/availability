import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AvailabilityAggregateService } from '../../aggregates/availability-aggregate/availability-aggregate.service';
import { RetrieveAvailabilityListQuery } from './retrieve-availability-list.query';

@QueryHandler(RetrieveAvailabilityListQuery)
export class RetrieveAvailabilityListHandler
  implements IQueryHandler<RetrieveAvailabilityListQuery> {
  constructor(private readonly manager: AvailabilityAggregateService) {}

  async execute(query: RetrieveAvailabilityListQuery) {
    const { offset, limit, search, sort, filter, clientHttpRequest } = query;
    return await this.manager.getItemList(
      Number(offset),
      Number(limit),
      search,
      sort,
      filter,
      clientHttpRequest,
    );
  }
}
