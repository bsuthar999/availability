import {
  ConfigService,
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_NAME,
  MONGO_URI_PREFIX,
} from './config.service';
import { MongoConnectionOptions } from 'typeorm/driver/mongodb/MongoConnectionOptions';
import { Availability } from '../availability/entity/availability/availability.entity';
export const DEFAULT = 'default';

export function connectTypeORM(config: ConfigService): MongoConnectionOptions {
  const mongoUriPrefix = config.get(MONGO_URI_PREFIX) || 'mongodb';

  return {
    name: DEFAULT,
    url: `${mongoUriPrefix}://${config.get(DB_USER)}:${config.get(
      DB_PASSWORD,
    )}@${config.get(DB_HOST)}/${config.get(DB_NAME)}`,
    type: 'mongodb',
    logging: false,
    synchronize: true,
    entities: [Availability],
    useNewUrlParser: true,
    w: 'majority',
    useUnifiedTopology: true,
  };
}
