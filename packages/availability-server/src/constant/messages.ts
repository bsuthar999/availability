export const AVAILABILITY_ALREADY_EXISTS = 'Provided time-slot already exists';
export const FROM_TIME_SHOULD_BE_LESS_THAN_TO_TIME =
  'Start time should be before End time';
