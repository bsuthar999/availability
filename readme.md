## Hello, Here are few quick overview for the above project.

----

## Stack and Description

* Lerna to manager our packages

* GitlabCi for our testing and CI.

* We are using angular as our frontend with material components.

* NestJs + MongoDb as our backend (thought of just using a static constant to do so but, why not a DB).

* We will try to keep it as simple and quick as possible.

----
### Our CI.

> https://gitlab.com/bsuthar999/availability/-/pipelines


### Here is a quick demo of how things work.

> https://gitlab.com/bsuthar999/availability/-/wikis/Quick-Demo.

### Here are the steps if you want to try it locally.

> https://gitlab.com/bsuthar999/availability/-/wikis/How-to-get-started.


**Note:** Due to the lack of time i was not able to achieve the 2nd point, ie create time slot for multiple dates.

